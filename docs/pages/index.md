title: Overview

The libGridXC library started life as SiestaXC, a collection of
modules within Siesta to compute the exchange-correlation energy and
potential in DFT calculations for atomic and periodic systems. The
''grid'' part of the name refers to the discretization for charge
density and potential used in those calculations. The original code
included a set of low-level routines to compute \(\epsilon_{xc}({\bf
r})\) and \(V_{xc}({\bf r})\) at a point for LDA and GGA functionals
(i.e., a subset of the functionality now offered by LibXC), and two
high-level routines to handle the computations in the whole domain,
with radial or 3D-periodic grids), including any needed computations
of gradients, integrations, etc.

In addition, SiestaXC pioneered the implementation of efficient and
practical algorithms for support of van der Waals functionals, which
are typically the sum of three pieces: an exchange energy from a
suitable GGA functional, an LDA correlation energy, and a non-local
correlation term

$$\begin{align}
E_{\mathrm{xc}}[n] = E_{\mathrm x}^{\mathrm{GGA}}[n] + E_{\mathrm
c}^{\mathrm{LDA}}[n] + E_{\mathrm{c}}^{\mathrm{nl}}[n].
\end{align}
$$

The last term is a double integral over a kernel function
\(\phi(\mathbf r, \mathbf r')\):

$$
\begin{align}
E_{\mathrm{c}}^{\mathrm{nl}}[n] = \frac12 \iint
n(\mathbf r) \phi(\mathbf r, \mathbf r') n(\mathbf r') \, \mathrm d
\mathbf r \, \mathrm d \mathbf r'.
\end{align}
$$

whose computation was made affordable by the techniques described in
[the work by Roman-Perez and Soler](https://link.aps.org/doi/10.1103/PhysRevLett.103.096102).

The current libGridXC retains most of the SiestaXC functionality, and
enhances it by offering an interface to [LibXC](https://www.tddft.org/programs/libxc/)
that supports a much wider selection of XC functionals. The code has been streamlined and
re-packaged into a proper stand-alone library, with an automatic
build-system.


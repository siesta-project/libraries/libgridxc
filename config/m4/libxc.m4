## Copyright (C) 2010-2015 M. Marques, X. Andrade, D. Strubbe, M. Oliveira
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
## Edit: Nick R. Papior
## Changed interface checks for only f90 and f03 versions.
##

AC_DEFUN([ACX_LIBXC], [
acx_libxc_ok=no
acx_libxc_vf90=no
acx_libxc_vf03=no

dnl Check if the library was given in the command line
dnl if not, use environment variables or defaults
AC_ARG_WITH(libxc, [AS_HELP_STRING([--with-libxc=DIR|yes|no], [Directory where libxc was installed.])])

# Set FCFLAGS_LIBXC only if not set from environment
if test x"$FCFLAGS_LIBXC" = x; then
  case $with_libxc in
    ""|yes) FCFLAGS_LIBXC="-I/usr/include" ;;
    no|none|NO|NONE) with_libxc="" ;;
    *)  FCFLAGS_LIBXC="-I$with_libxc/include" ;;
  esac
fi

AC_ARG_WITH(libxc-include, [AS_HELP_STRING([--with-libxc-include=DIR], [Directory where libxc Fortran headers were installed.])])
case $with_libxc_include in
  "") ;;
  *)  FCFLAGS_LIBXC="-I$with_libxc_include" ;;
esac

dnl Backup LIBS and FCFLAGS
acx_libxc_save_LIBS="$LIBS"
acx_libxc_save_FCFLAGS="$FCFLAGS"

testprogf90="AC_LANG_PROGRAM([],[
  use xc_f90_lib_m
  implicit none
  integer :: major
  integer :: minor
  integer :: micro
  ! this will not work in pre-3.0 versions
  ! since the 'micro' argument is missing
  ! Comment out and depend only on module existence for
  ! success.
  !! call xc_f90_version(major, minor, micro)])"

testprogf03="AC_LANG_PROGRAM([],[
  use xc_f03_lib_m
  implicit none
  integer :: major
  integer :: minor
  integer :: micro
  integer :: flags = XC_FLAGS_NEEDS_LAPLACIAN
  call xc_f03_version(major, minor, micro)])"

FCFLAGS="$FCFLAGS_LIBXC $acx_libxc_save_FCFLAGS"

# set from environment variable, if not blank
if test ! -z "$LIBS_LIBXC"; then
  LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
  AC_MSG_CHECKING([for libxc LIBS_LIBXC])
  AC_LINK_IFELSE($testprog, [acx_libxc_ok=yes], [])
  AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
fi

# dynamic linkage, f03
if test x"$acx_libxc_ok" = xno; then
  AC_MSG_CHECKING([for libxc f03 (dynamic)])
  if test ! -z "$with_libxc"; then
    LIBS_LIBXC="-L$with_libxc/lib"
  else
    LIBS_LIBXC=""
  fi
  LIBS_LIBXC="$LIBS_LIBXC -lxcf03 -lxc"
  LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
  AC_LINK_IFELSE($testprogf03, [acx_libxc_ok=yes; acx_libxc_vf03=yes], [])
  AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
fi

# dynamic linkage, f90
if test x"$acx_libxc_ok" = xno; then
  AC_MSG_CHECKING([for libxc f90 (dynamic)])
  if test ! -z "$with_libxc"; then
    LIBS_LIBXC="-L$with_libxc/lib"
  else
    LIBS_LIBXC=""
  fi
  LIBS_LIBXC="$LIBS_LIBXC -lxcf90 -lxc"
  LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
  AC_LINK_IFELSE($testprogf90, [acx_libxc_ok=yes; acx_libxc_vf90=yes], [])
  AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
fi

if test ! -z "$with_libxc"; then
  # static linkage, f03
  if test x"$acx_libxc_ok" = xno; then
    AC_MSG_CHECKING([for libxc f03 (static)])
    LIBS_LIBXC="$with_libxc/lib/libxcf03.a $with_libxc/lib/libxc.a"
    LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
    AC_LINK_IFELSE($testprogf03, [acx_libxc_ok=yes; acx_libxc_vf03=yes], [])
    AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
  fi

  # static linkage, f90
  if test x"$acx_libxc_ok" = xno; then
    AC_MSG_CHECKING([for libxc f90 (static)])
    LIBS_LIBXC="$with_libxc/lib/libxcf90.a $with_libxc/lib/libxc.a"
    LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
    AC_LINK_IFELSE($testprogf90, [acx_libxc_ok=yes; acx_libxc_vf90=yes], [])
    AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
  fi

fi



# check for easybuild
if test x"$acx_libxc_ok" = xno; then
  AC_MSG_CHECKING([for libxc through EasyBuild])
  if test -n "${EBROOTLIBXC}"; then
    FCFLAGS_LIBXC="-I$EBROOTLIBXC/include"
    FCFLAGS="$FCFLAGS_LIBXC $acx_libxc_save_FCFLAGS"
    LIBS_LIBXC="-L${EBROOTLIBXC}/lib -lxcf03 -lxc"
    LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
    AC_LINK_IFELSE($testprogf03, [acx_libxc_ok=yes; acx_libxc_vf03=yes], [])

    if test x"$acx_libxc_ok" = xno; then
      LIBS_LIBXC="-L${EBROOTLIBXC}/lib -lxcf90 -lxc"
      LIBS="$LIBS_LIBXC $acx_libxc_save_LIBS"
      AC_LINK_IFELSE($testprogf90, [acx_libxc_ok=yes; acx_libxc_vf90=yes], [])
    fi
  fi
  AC_MSG_RESULT([$acx_libxc_ok ($FCFLAGS_LIBXC $LIBS_LIBXC)])
fi


dnl Finally, execute ACTION-IF-FOUND/ACTION-IF-NOT-FOUND:
if test x"$acx_libxc_ok" = xyes; then
  AC_DEFINE(HAVE_LIBXC, 1, [Defined if you have the LIBXC library.])
else
  AC_MSG_NOTICE([Could not find required libxc library ( >= 3.0.0).])
  FCFLAGS_LIBXC=""
  LIBS_LIBXC=""
  FCFLAGS="$acx_libxc_save_FCFLAGS"
  LIBS="$acx_libxc_save_LIBS"
fi

AC_MSG_CHECKING([whether libxc uses f90 interface])
AC_MSG_RESULT([$acx_libxc_vf90])

AC_MSG_CHECKING([whether libxc uses f03 interface])
AC_MSG_RESULT([$acx_libxc_vf03])

AC_SUBST(FCFLAGS_LIBXC)
AC_SUBST(LIBS_LIBXC)
FCFLAGS="$acx_libxc_save_FCFLAGS"
LIBS="$acx_libxc_save_LIBS"
])dnl ACX_LIBXC

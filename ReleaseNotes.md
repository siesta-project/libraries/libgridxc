# Release Notes for LibGridXC

* Fix logarithmic grid detection in mesh1d (for vdw functionals)

* Procedure pointers are now used for the allocation, timing, and error reporting
  events. Built-in default associations are provided, but client codes can install
  their own versions. The library does not need 'external handler' routines any more.

* Create CMake framework.

* Use gridxc_ prefix in module names.

* Create autotools framework.

* (for earlier developments, see the legacy ChangeLog)


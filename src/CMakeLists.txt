set(dir "${CMAKE_CURRENT_SOURCE_DIR}")

list(
  APPEND srcs

  "${dir}/alloc.F90"
  "${dir}/am05.F90"
  "${dir}/array.F90"
  "${dir}/atomxc.F90"
  "${dir}/bessph.F90"
  "${dir}/cellsubs.F90"
  "${dir}/cellxc.F90"
  "${dir}/chkgmx.F90"
  "${dir}/debugxc.F90"
  "${dir}/fft3d.F90"
  "${dir}/fftr.F90"
  "${dir}/ggaxc.F90"
  "${dir}/gridxc.F90"
  "${dir}/gridxc_config.F90"
  "${dir}/gridxc_fft_gpfa.F90"
  "${dir}/interpolation.F90"
  "${dir}/ldaxc.F90"
  "${dir}/m_io.F90"
  "${dir}/m_walltime.F90"
  "${dir}/mesh1d.F90"
  "${dir}/mesh3d.F90"
  "${dir}/minvec.F90"
  "${dir}/moreParallelSubs.F90"
  "${dir}/precision.F90"
  "${dir}/radfft.F90"
  "${dir}/sorting.F90"
  "${dir}/sys.F90"
  "${dir}/vdwxc.F90"
  "${dir}/vv_vdwxc.F90"
  "${dir}/xc_hybrids.F90"
  "${dir}/xc_xwpbe.F90"
  "${dir}/xcmod.F90"

)

set(srcs "${srcs}" PARENT_SCOPE)
